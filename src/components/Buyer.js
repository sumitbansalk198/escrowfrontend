import React from 'react'
import { useState } from 'react'
import Notification from './Notification';


export default function Buyer() {

    let msg

    const [buy, setbuy] = useState("")
    const [sell, setsell] = useState("")
    const [id, setid] = useState('')
    const [payment, setpayment] = useState('')
    const [buyerBalance, setBuyerBalance] = useState('')
    const [sellerBalance, setSellerBalance] = useState('')
    const [sellerNotification, setSellerNotification] = useState([])
    const [buyerNotification, setBuyerNotification] = useState([])
    const [payoutStatus, setpayoutStatus] = useState('NO')
    const [Deliveryprocess, setDeliveryProcess] = useState('NO')
    const [DeliveryConfirm, setDeliveryConfirm] = useState('NO')
    const [buyerName, setBuyerName] = useState('')
    const [buyerEmail, setBuyerEmail] = useState('')
    const [buyerPassword, setBuyerPassword] = useState('')
    const [sellerName, setSellerName] = useState('')
    const [sellerEmail, setSellerEmail] = useState('')
    const [sellerPassword, setSellerPassword] = useState('')
    const [returnPayout, setReturnPayout] = useState('NO')

    const generateContract = () => {
        if (buy !== sell && buy !== '' && sell !== '') {


            const generateId = () => {

                if (id !== '' && id!=='Invalid Address') {
                    return (
                        <div className='mb-3' style={{display:'flex', justifyContent:'space-between' }}>
                            <div style={{width:'40%', backgroundColor:'orange',border: '1px solid black'}}>Contract Address</div>
                            <div className='' style={{ border: '1px solid black', width:'50%' }}><center>{id}</center></div>
                        </div>

                    )
                }else if(id==='Invalid Address'){
                    return(
                        <div className='bg-danger' style={{color:'white'}}>{id}</div>
                    )
                }
            }
            return (
                <center className='container'><button className='mb-3 mt-3' type='Submit' style={{ backgroundColor: 'orange' }} onClick={
                    (e) => {
                        e.preventDefault()
                        let url = `http://127.0.0.1:5000/api/constructor/${buy}/${sell}`

                        fetch(url)
                            .then(e => e.json())
                            .then(e => {
                                setid(e[0])
                                setBuyerBalance(e[1])
                                setSellerBalance(e[2])
                            })
                            .catch(e => console.log(e))

                        msg = 'Contract has Generated'

                        setBuyerNotification(previousState => {
                            if ([...previousState].includes(msg)) {
                                return [...previousState]
                            } else {
                                return [...previousState, msg]
                            }
                        })

                        setSellerNotification(previousState => {
                            if ([...previousState].includes(msg)) {
                                return [...previousState]
                            } else {
                                return [...previousState, msg]
                            }
                        })

                    }
                }>Create Contract</button>
                    {generateId()}
                </center>
            )
        }
    }

    const makepayment = () => {
        if ((id !== '') && (id !== 'Address are wrong')) {
            return (
                <div style={{ display: 'flex', justifyContent: 'space-between' }} >
                    <button className="mb-3 mt-3" type='Submit' style={{ backgroundColor: 'orange', width: '40%' }} onClick={
                        (e) => {
                            e.preventDefault()
                            let url = `http://127.0.0.1:5000/api/confirmpayment/${payment}`

                            fetch(url)
                                .then(e => e.json())
                                .then(e => {
                                    console.log(e[0])
                                    setpayoutStatus(e[1])
                                })
                                .catch(e => console.log(e))

                            msg = `Payment of ${payment} ether has been done`

                            setSellerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                            // setpayoutStatus('YES')
                        }
                    }><center>Make Payment</center></button>
                    <input type="text" className='mb-3 mt-3 ' value={payment} style={{ width: '50%' }} onChange={e => { setpayment(e.target.value) }}></input>
                    {/* <div className='mb-3' style={{ border: '1px solid black' }} >
                        <center>{payment}</center></div> */}
                </div>
            )
        }
    }

    const paymentstatus = () => {

        if (id !== '' && (id !== 'Address are wrong')) {

            return (
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
                    <div className='mb-3 ' style={{ border: '1px solid black', width: '90%' }} ><center>Payment Status</center></div>
                    <div className='mb-3' style={{ border: '1px solid black', width: '90%' }} >
                        <center>{payoutStatus}</center></div>
                </div>
            )
        }
    }

    const delivery = () => {

        if (payoutStatus === 'YES') {

            return (
                <div style={{ display: 'flex', justifyContent: 'space-between' }} >
                    <button className="mb-3 mt-3" type='Submit' style={{ backgroundColor: 'orange', width: '40%' }} onClick={
                        (e) => {
                            e.preventDefault()
                            msg = 'Delivery is proceeded'

                            setSellerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                            setBuyerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                            setDeliveryProcess('YES')
                        }
                    }><center>Proceed Delivery</center></button>
                    <div className='mb-3 mt-3' style={{ border: '1px solid black', width: '50%', backgroundColor: 'white' }} >
                        <center>{Deliveryprocess}</center>
                    </div>
                </div>
            )
        }
    }

    const deliveryconfirmation = () => {

        if (Deliveryprocess === 'YES' && returnPayout === 'NO') {
            return (
                <div style={{ display: 'flex', justifyContent: 'space-between' }} >
                    <button className="mb-3 mt-3" type='Submit' style={{ backgroundColor: 'orange', width: '45%' }} onClick={
                        (e) => {

                            e.preventDefault()

                            fetch("http://127.0.0.1:5000/api/confirmdelivery")
                                .then(e => e.json())
                                .then(e => {
                                    console.log(e[0])
                                    setDeliveryConfirm(e[1])
                                })
                                .catch(e => console.log(e))


                            msg = 'Delivery is confirmed'

                            setSellerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg, `You received an amount of ${payment}`]
                                }
                            })

                            setBuyerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                            // setDeliveryConfirm('YES')
                        }
                    }><center>Delivery Confirmation</center></button>
                    <div className='mb-3 mt-3' style={{ border: '1px solid black', width: '50%', backgroundColor: 'white' }} >
                        <center>{DeliveryConfirm}</center>
                    </div>
                </div>
            )
        }
    }

    const returnPayment = () => {

        if (payoutStatus === 'YES' && DeliveryConfirm === 'NO') {

            return (
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
                    <button className="mb-3 mt-3" type='Submit' style={{ backgroundColor: 'orange', width: '90%' }} onClick={
                        e => {
                            e.preventDefault()
                            fetch("http://127.0.0.1:5000/api/returnpayment")
                                .then(e => e.json())
                                .then(e => {
                                    console.log(e[0])
                                    setReturnPayout(e[1])
                                })
                                .catch(e => console.error(e))

                            msg = 'Payment is return to buyer'

                            setSellerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                            setBuyerNotification(previousState => {
                                if ([...previousState].includes(msg)) {
                                    return [...previousState]
                                } else {
                                    return [...previousState, msg]
                                }
                            })

                        }
                    }><center>Return Payment</center></button>
                    <div className='mb-3' style={{ border: '1px solid black', width: '90%' }} >
                        <center>{returnPayout}</center></div>
                </div>
            )
        }
    }


    return (
        <>
            <div className='container mt-4' style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{ width: '40%', backgroundColor: 'lightgray', border: '1px solid black', justifyContent: 'center', display: 'flex', height: 'auto' }}>
                    <center style={{ width: '90%' }}>
                        <div className='mb-3 mt-3' style={{ height: '10%', backgroundColor: 'orange', border: '1px solid black', }}>
                            <center>BUYER</center>
                        </div>

                        <form >
                            <div className="mb-3">
                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>NAME</center></div>
                                    <input type="text" value={buyerName} onChange={e => { setBuyerName(e.target.value) }} style={{ width: '60%', height: '10%' }}></input>
                                </div>

                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>EMAIL</center></div>
                                    <input type="email" style={{ width: '60%', height: '10%' }} value={buyerEmail} onChange={e => setBuyerEmail(e.target.value)}></input>
                                </div>

                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>PASSWORD</center></div>
                                    <input type="password" style={{ width: '60%', height: '10%' }} value={buyerPassword} onChange={e => setBuyerPassword(e.target.value)}></input>
                                </div>

                                <button type="submit" className="btn" style={{ backgroundColor: 'orange', border: '1px solid black' }} onClick={
                                    (e) => {

                                        e.preventDefault()
                                        if (!buyerName || !buyerEmail || !buyerPassword) {
                                            alert('Value cannot be blank')
                                        } else {
                                            setBuyerName("");
                                            setBuyerEmail("");
                                            setBuyerPassword('')
                                            msg = 'Your Wallet is Created'

                                            setBuyerNotification(previousState => {
                                                if ([...previousState].includes(msg)) {
                                                    return [...previousState]
                                                } else {
                                                    return [...previousState, msg]
                                                }
                                            })

                                            fetch("http://127.0.0.1:5000/api/createwallet")
                                                .then(e => e.json())
                                                .then(e => setbuy(e))
                                                .catch(e => console.error(e))

                                            // setbuy(hash[Math.floor(Math.random() * hash.length)])
                                        }
                                    }}>Create Wallet</button>
                                {/* {buyhash()} */}
                                <input className=' mt-3 mb-3' type="text" value={buy} onChange={e => { setbuy(e.target.value) }} style={{ width: '100%', height: '10%' }}></input>
                                <Notification Notification={buyerNotification} />
                                <div className='mt-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'orange' }}><center>Balance</center></div>
                                    <div style={{ width: '60%', display: 'flex', height: '10%' }}>
                                        <div style={{ width: '60%', border: '1px solid black', backgroundColor: 'white' }}><center>{buyerBalance}</center></div>
                                        <div style={{ width: '40%', border: '1px solid black', backgroundColor: 'orange' }}>Ether</div>
                                    </div>
                                </div>

                                {makepayment()}
                                {deliveryconfirmation()}


                            </div>
                        </form>
                    </center>

                </div>

                <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', width: '20%' }}>


                    {paymentstatus()}
                    {returnPayment()}

                </div>

                <div style={{ width: '40%', border: '1px solid black', backgroundColor: 'lightgray', justifyContent: 'center', display: 'flex' }}>
                    <center style={{ width: '90%' }}>
                        <div className='mb-3 mt-3' style={{ height: '10%', backgroundColor: 'orange', border: '1px solid black', }}>
                            <center>SELLER</center>
                        </div>

                        <form onSubmit={(e) => {
                            e.preventDefault();
                            if (!sellerName || !sellerEmail || !sellerPassword) {
                                alert('Value cannot be blank')
                            } else {
                                setSellerName("");
                                setSellerEmail("");
                                setSellerPassword('')
                            }
                        }}>
                            <div className="mb-3">
                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>NAME</center></div>
                                    <input type="text" style={{ width: '60%', height: '10%' }} value={sellerName} onChange={e => setSellerName(e.target.value)}></input>
                                </div>

                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>EMAIL</center></div>
                                    <input type="email" style={{ width: '60%', height: '10%' }} value={sellerEmail} onChange={e => setSellerEmail(e.target.value)}></input>
                                </div>

                                <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'white' }}><center>PASSWORD</center></div>
                                    <input type="password" style={{ width: '60%', height: '10%' }} value={sellerPassword} onChange={e => setSellerPassword(e.target.value)}></input>
                                </div>

                                <button type="submit" className="btn" style={{ backgroundColor: 'orange', border: '1px solid black' }} onClick={
                                    (e) => {

                                        e.preventDefault()
                                        if (!sellerName || !sellerEmail || !sellerPassword) {
                                            alert('Value cannot be blank')
                                        } else {
                                            setSellerName("");
                                            setSellerEmail("");
                                            setSellerPassword('')

                                            msg = 'Your Wallet has Created'

                                            setSellerNotification(previousState => {
                                                if ([...previousState].includes(msg)) {
                                                    return [...previousState]
                                                } else {
                                                    return [...previousState, msg]
                                                }
                                            })

                                            fetch("http://127.0.0.1:5000/api/createwallet")
                                                .then(e => e.json())
                                                .then(e => setsell(e))
                                                .catch(e => console.error(e))
                                        }


                                        // setsell(hash[Math.floor(Math.random() * hash.length)])
                                    }}
                                >Create Wallet</button>
                                {/* {sellhash()} */}
                                <input className=' mt-3 mb-3' type="text" value={sell} onChange={e => { setsell(e.target.value) }} style={{ width: '100%', height: '10%' }}></input>
                                <Notification Notification={sellerNotification} />
                                <div className='mt-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div style={{ width: '30%', height: '10%', border: '1px solid black', backgroundColor: 'orange' }}><center>Balance</center></div>
                                    <div style={{ width: '60%', display: 'flex', height: '10%' }}>
                                        <div style={{ width: '60%', border: '1px solid black', backgroundColor: 'white' }}><center>{sellerBalance}</center></div>
                                        <div style={{ width: '40%', border: '1px solid black', backgroundColor: 'orange' }}>Ether</div>
                                    </div>
                                </div>
                                {delivery()}

                            </div>
                        </form>

                    </center>
                </div>

            </div>
            {generateContract()}
        </>
    )
}